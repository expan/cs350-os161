/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * Sample/test code for running a user program.  You can use this for
 * reference when implementing the execv() system call. Remember though
 * that execv() needs to do more than this function does.
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <lib.h>
#include <proc.h>
#include <current.h>
#include <addrspace.h>
#include <vm.h>
#include <vfs.h>
#include <syscall.h>
#include <test.h>
#include <limits.h>
#include "opt-A2.h"
#include <copyinout.h>


/*
 * Load program "progname" and start running it in usermode.
 * Does not return except on error.
 *
 * Calls vfs_open on progname and thus may destroy it.
 */
int
runprogram(char *progname, char ** args, unsigned long argCount)
{
	struct addrspace *as;
	struct vnode *v;
	vaddr_t entrypoint, stackptr, argPtr; // these are just integers
	int result, offset;

	/* Open the file. */
	result = vfs_open(progname, O_RDONLY, 0, &v);
	if (result) {
		return result;
	}

	/* We should be a new process. */
	// KASSERT(curproc_getas() == NULL);

	/* Create a new address space. */
	as = as_create();
	if (as == NULL) {
		vfs_close(v);
		return ENOMEM;
	}

	/* Deactiveate the current address space, if exists */
	if (curproc_getas() != NULL) {
		as_deactivate();
	}

	/* Switch to it and activate it (clean up old one). */
	struct addrspace * oldas = curproc_setas(as);
	if (oldas != NULL) {
		as_destroy(oldas);
	}
	as_activate();

	/* Load the executable. */
	result = load_elf(v, &entrypoint);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		vfs_close(v);
		return result;
	}

	/* Done with the file now. */
	vfs_close(v);

	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {
		/* p_addrspace will go away when curproc is destroyed */
		return result;
	}

	// New stuff
	#if OPT_A2

	// copy arg strings to the stack of user address space
	// char ** type since last element of array must be NULL
    char ** tempPtr = kmalloc((argCount+1)*sizeof(char *)); // + 1 for last item to be NULL
    
    for (unsigned int i=0; i < argCount; i++){
        int len = strlen(args[i]) + 1; // + 1 for '\0'
        // Move back the stackptr by length of the arg
        stackptr -= len;
        // Copy argument strings onto stack
        result = copyout(args[i], (userptr_t)stackptr, len);  
        // Copying error
        if (result) {
          kfree(tempPtr);
          return result;
        }
        // Also put the address into tempPtr
        tempPtr[i] = (char*) stackptr;
    }
    // Set last thing to NULL
    tempPtr[argCount] = NULL;
    

    // Make sure the string stack is padded to 4 byte aligned
    offset = stackptr%4;
    stackptr -= offset;
    // Zero out the offset of the stack amount
    bzero((void *)stackptr, offset);


    // Set stackptr back to store argument array onto the stack
    offset = (argCount+1)*sizeof(char*);
    stackptr -= offset;
    
    // copy temp ptr over to stack ptr
    result = copyout(tempPtr, (userptr_t)stackptr, offset);    
    if (result) {
    	kfree(tempPtr);
	    return result;
    }

    // Set argument pointer to where stack pointer currently is
    argPtr = stackptr;

    // Pad item to be 8-byte aligned
    offset = stackptr%8;
    stackptr -= offset;
    //zero out offset of the stack amount
    bzero((void *)stackptr, offset);

    // free stuff used
    kfree(tempPtr);

    // warp to user mode
    enter_new_process(argCount /*argc*/, (userptr_t)argPtr /*userspace addr of argv*/,
         stackptr, entrypoint); 	
	
	#else
	/* Warp to user mode. */
	enter_new_process(0 /*argc*/, NULL /*userspace addr of argv*/,
			  stackptr, entrypoint);
	#endif
	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
}
