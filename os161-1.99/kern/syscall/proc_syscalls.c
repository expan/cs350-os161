#include <types.h>
#include <kern/errno.h>
#include <kern/unistd.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <current.h>
#include <proc.h>
#include <thread.h>
#include <addrspace.h>
#include <copyinout.h>
#include "opt-A2.h"
#include <synch.h>
#include <mips/trapframe.h>
#include <limits.h>
#include <test.h>

// ARG_MAX / PATH_MAX
#define NUM_ARGS_MAX 64

  /* this implementation of sys__exit does not do anything with the exit code */
  /* this needs to be fixed to get exit() and waitpid() working properly */

void sys__exit(int exitcode) {
#if OPT_A2
  struct addrspace *as;
  struct proc *p = curproc;
  /* for now, just include this to keep the compiler from complaining about
     an unused variable */
 
  KASSERT(curproc->p_addrspace != NULL);

  as_deactivate();
  /*
   * clear p_addrspace before calling as_destroy. Otherwise if
   * as_destroy sleeps (which is quite possible) when we
   * come back we'll be calling as_activate on a
   * half-destroyed address space. This tends to be
   * messily fatal.
   */
  as = curproc_setas(NULL);
  as_destroy(as);


  proc_remthread(curthread);

  // Set exitcode and status
  p->p_exited = true;
  p->p_exitcode = _MKWAIT_EXIT(exitcode);
  // If parent is waiting on this, then wake it up
  lock_acquire(p->p_wait_lk);
  cv_broadcast(p->p_wait_cv, p->p_wait_lk);
  lock_release(p->p_wait_lk);

  // If there is no parent we are jsut free to destroy it
  if(p->p_parent == NULL){
    proc_destroy(p);
  }else{
    // else we must move this curproc to its parents dead childs array
    struct proc *par = p->p_parent;

    lock_acquire(par->p_wait_lk);
    //Search for the curthread in it's parents p_alive_children array
    for(unsigned int i=0; i<array_num(par->p_alive_children); i++){
      struct proc *curChild = array_get(par->p_alive_children, i);
      if(curChild->pid == p->pid){
        //When found, remove from p_alive_children and add it to the p_dead_children
        array_remove(par->p_alive_children, i);
        array_add(par->p_dead_children, p, NULL);
        break;
      }
    }
    lock_release(par->p_wait_lk);
  }
  thread_exit();
  /* thread_exit() does not return, so we should never get here */
  panic("return from thread_exit in sys_exit\n");
  #endif
}

#if OPT_A2
// Call execv to switch user programs 
int sys_execv(const_userptr_t prog, userptr_t args){
 
  unsigned long argCount = 0;

  size_t progLen = strlen((char *)prog) + 1; // + 1 for '\0'
  size_t totalLen = 0;
  size_t pathLen;

  // Count arguments and the total arguments length
  for(;((userptr_t *)args)[argCount] != NULL; argCount++) {
    totalLen += strlen(((char **)args)[argCount]) + 1; // + 1 for '\0'
  }

  // Need to check if this is right
  if (progLen + totalLen > ARG_MAX || argCount > NUM_ARGS_MAX) {
    // Args too big
    return E2BIG;
  }
  
  char kprog[PATH_MAX];
  char ktempArgs[totalLen]; // long string containing all args, for a temporary storage
  char *kargs[argCount]; // array of strings similar to args, but for kernel
  int numCopied = 0; // number of characters copied so far

  // Copy program name into kprog
  int cpyres = copyinstr(prog, kprog, PATH_MAX, &pathLen);
  if (cpyres) {
    // Copy error
    return cpyres;
  }
  if(pathLen == 1){
    return ENOENT;
  }
  
  // Copy user arguments into kernel address space
  for (unsigned long i = 0; i < argCount; i++) {
    size_t argLen = strlen(((char **)args)[i]) + 1; // + 1 for '\0'
    
    // ktempArgs is one long string, so use an offset counter to know where to copy to next
    cpyres = copyinstr(((userptr_t *)args)[i], (char *)(ktempArgs + numCopied), argLen, &pathLen);
    if (cpyres) {
      return cpyres;
    }
    // then set the args array to what was copied
    kargs[i] = ktempArgs + numCopied;
    // Update offset counter 
    numCopied += pathLen;
  }

  // Do the rest of switching programs by calling runprogram 
  int err = runprogram(kprog,kargs,argCount);

  // Should not return, unless an error occurs

  return err;
  
}

// Sys_fork function to fork a process and create a child of it
// Returns 0 for the child and the pid of the child for the parent

int sys_fork(struct trapframe *ctf, pid_t *retval){

  KASSERT(curproc != NULL);

  struct proc *new = proc_create_runprogram(curproc->p_name);
  if(new == NULL){
//    DEBUG(DB_SYSCALL, "sys_fork can't create child");
    return ENOMEM; // no memory left
  }
  
  // genpid returns -1 if there's a problem so check for emproc
  if(new->pid < 0){
    proc_destroy(new);
    return EMPROC;
  }
  
  // Copy address space
  spinlock_acquire(&new->p_lock);
  as_copy(curproc_getas(), &(new->p_addrspace));
  spinlock_release(&new->p_lock);

  if(new->p_addrspace == NULL){
    DEBUG(DB_SYSCALL, "sys_fork could not create new address space");
    proc_destroy(new); // destroy since it fayiled
    return ENOMEM; // out of memory for addr space
  }

  // Create duplicate trapframe
  struct trapframe *tf_child = kmalloc(sizeof(struct trapframe));
  if(tf_child == NULL){
    DEBUG(DB_SYSCALL, "sys_fork could not create new trapframe space");
    proc_destroy(new);
    return ENOMEM; // out of memory for trap space
  }
  memcpy(tf_child, ctf, sizeof(struct trapframe));

//Set p_parent field to curproc and add new proc to curproc's alive children and all children
  lock_acquire(curproc->p_wait_lk);
  new->p_parent = curproc;
  array_add(curproc->p_alive_children, new, NULL);
  array_add(curproc->p_all_children,new,NULL);
  lock_release(curproc->p_wait_lk);

  // Thread fork call
  int result = thread_fork(curthread->t_name, new, enter_forked_process, (struct trapframe *)tf_child, 0);
  if(result){
    proc_destroy(new);
    kfree(tf_child);
    DEBUG(DB_SYSCALL, "sys_fork thread forking failed");
    return ENOMEM;
  }

  // Set return value
  *retval = new->pid;
 // #endif
  return 0;
}
#endif
/* stub handler for getpid() system call                */
int
sys_getpid(pid_t *retval)
{
  /* for now, this is just a stub that always returns a PID of 1 */
  /* you need to fix this to make it work properly */
 // *retval = 1;
  #if OPT_A2
    KASSERT(curproc != NULL);
    *retval = curproc->pid;
  #else
    *retval = 1;
  #endif
  return(0);
}

/* stub handler for waitpid() system call                */

int
sys_waitpid(pid_t pid,
      userptr_t status,
      int options,
      pid_t *retval)
{
  int exitstatus;
  int result;

  // Check for problems at the start
  if(pid < PID_MIN || pid > PID_MAX)
  if (options != 0) {
    *retval = -1;
    return EINVAL;
  }
  if(status == NULL){
    *retval = -1;
    return EFAULT;
  }

  /* this is just a stub implementation that always reports an
     exit status of 0, regardless of the actual exit status of
     the specified process.   
     In fact, this will return 0 even if the specified process
     is still running, and even if it never existed in the first place.

     Fix this!
  */
  #if OPT_A2

  // CHeck if there is such a child of curproc with pid value of pid
  struct proc *child = proc_find_by_pid(curproc,pid);

  if(child == NULL){
    *retval = -1;
    return ECHILD;
  }

  // If the child has not exited yet, cv_wait until then
  lock_acquire(child->p_wait_lk);
  while(!child->p_exited){
    cv_wait(child->p_wait_cv, child->p_wait_lk);
  }
  lock_release(child->p_wait_lk);
  
  // set exit status
  exitstatus = child->p_exitcode;
  #else
   exitstatus = 0;
  #endif
  result = copyout((void *)&exitstatus,status,sizeof(int));
  if (result) {
    *retval = -1;
    return(result);
  }
  *retval = pid;
  return(0);
}


