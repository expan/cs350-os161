/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <proc.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include "opt-A3.h"
#include <syscall.h>

/*
 * Dumb MIPS-only "VM system" that is intended to only be just barely
 * enough to struggle off the ground.
 */

/* under dumbvm, always have 48k of user stack */
#define DUMBVM_STACKPAGES    12


static struct physicalPage *coremap = NULL;
static bool inStartup = true;
int totalPages;
paddr_t firstPage, lastPage;
static struct spinlock coremap_lock = SPINLOCK_INITIALIZER;

/*
 * Wrap ram_stealmem in a spinlock.
 */
static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;

void vm_bootstrap(void) {
	#if OPT_A3
	ram_getsize(&firstPage, &lastPage);

	// Calc how many pages needed
	totalPages = (lastPage - firstPage) / PAGE_SIZE;

	// Calculate coremap's actual memory size
	int coremapSize = sizeof(struct physicalPage) * totalPages;

	// Add PAGE_SIZE - 1 to coremapsize so that always round up  
	// Should probably use roundup
	int pagesNeededCoremap = (coremapSize + PAGE_SIZE - 1)/PAGE_SIZE;

	paddr_t coremapAddr = ram_stealmem(pagesNeededCoremap);
	totalPages -= pagesNeededCoremap; // Pages used for coremap are now gone
	spinlock_acquire(&coremap_lock);
	// convert to physical address
	coremap = (struct physicalPage *)PADDR_TO_KVADDR(coremapAddr);

	// Initialize all coremap pages 
	for (int i = 0; i < totalPages; i++) {
		coremap[i].inUse = false;
		coremap[i].nextPage = -1;
	}
	spinlock_release(&coremap_lock);
	// Recalc ramsize again
	ram_getsize(&firstPage, &lastPage);
	ram_unset();

	inStartup = false;
	#endif
}

static
paddr_t
getppages(unsigned long npages)
{

	paddr_t addr;

	spinlock_acquire(&stealmem_lock);
	#if OPT_A3
	// If not in the start up stage
	if (!inStartup) {

		int start = -1;
		int prevPageIdx = -1;
		int i;
		spinlock_acquire(&coremap_lock);
		// Parse through all pages looking for free pages
		for (i = 0; i < totalPages && npages > 0; i++) {
			// If current page in use, skip
			if (coremap[i].inUse)
			 continue;

			coremap[i].inUse = true;

			// If the first page has been allocated already, set starting point
			if (prevPageIdx < 0) {
				start = i;
			//Otherwise set prev allocated page to point to current page;
			} else {
				coremap[prevPageIdx].nextPage = i;
			}

			// Set previous page
			prevPageIdx = i;

			npages--;
			if (npages == 0) 
				break;
		}

		// Otherwise if parsed through all the physical memory, panic
		if (i == totalPages) {
			panic("Allocating memory failed: All pages in use\n");
		}

		addr = (paddr_t)(firstPage + start * PAGE_SIZE);
		spinlock_release(&coremap_lock);
	//Initial start up so we steal memory
	} else
	#endif
		addr = ram_stealmem(npages);

	spinlock_release(&stealmem_lock);
	return addr;
}

/* Allocate/free some kernel-space virtual pages */
vaddr_t 
alloc_kpages(int npages)
{
	paddr_t pa;
	pa = getppages(npages);
	if (pa==0) {
		return 0;
	}
	return PADDR_TO_KVADDR(pa);
}

void 
free_kpages(vaddr_t addr)
{
	#if OPT_A3
	// Convert to paddr
	paddr_t paddr = KVADDR_TO_PADDR(addr);
	// Check for paddr to be page aligned
	KASSERT(paddr % PAGE_SIZE == 0); 
 
	// Convert to physical page index
	int pageIdx = (paddr - firstPage) / PAGE_SIZE;
	spinlock_acquire(&coremap_lock);
	// Parse through all allocated pages until end of block reached and free them
	while(pageIdx>=0){
		// Check if current page is in use
		KASSERT(coremap[pageIdx].inUse);

		// Set variables to initial value
		coremap[pageIdx].inUse = false;
		// Temp value for previous page index
		int temp = pageIdx;
		// Set page index to the next page pointed to
		pageIdx = coremap[temp].nextPage;
		// Set previous page index to -1 to free
		coremap[temp].nextPage = -1;
	} 
	spinlock_release(&coremap_lock);
	#endif
}

void
vm_tlbshootdown_all(void)
{
	panic("dumbvm tried to do tlb shootdown?!\n");
}

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("dumbvm tried to do tlb shootdown?!\n");
}

int
vm_fault(int faulttype, vaddr_t faultaddress)
{
	vaddr_t vbase1, vtop1, vbase2, vtop2, stackbase, stacktop;
	paddr_t paddr;
	int i;
	uint32_t ehi, elo;
	struct addrspace *as;
	int spl;
	bool dirtiable;

	faultaddress &= PAGE_FRAME;

	DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

	switch (faulttype) {
	    case VM_FAULT_READONLY:
		/* We always create pages read-write, so we can't get this */
	    	sys__exit(faulttype);
	    case VM_FAULT_READ:
	    case VM_FAULT_WRITE:
		break;
	    default:
		return EINVAL;
	}

	if (curproc == NULL) {
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = curproc_getas();
	if (as == NULL) {
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* Assert that the address space has been set up properly. */
	KASSERT(as->as_vbase1 != 0);
	KASSERT(as->as_pbase1 != 0);
	KASSERT(as->as_npages1 != 0);
	KASSERT(as->as_vbase2 != 0);
	KASSERT(as->as_pbase2 != 0);
	KASSERT(as->as_npages2 != 0);
	KASSERT(as->as_stackpbase != 0);
	KASSERT((as->as_vbase1 & PAGE_FRAME) == as->as_vbase1);
	KASSERT((as->as_pbase1 & PAGE_FRAME) == as->as_pbase1);
	KASSERT((as->as_vbase2 & PAGE_FRAME) == as->as_vbase2);
	KASSERT((as->as_pbase2 & PAGE_FRAME) == as->as_pbase2);
	KASSERT((as->as_stackpbase & PAGE_FRAME) == as->as_stackpbase);

	vbase1 = as->as_vbase1;
	vtop1 = vbase1 + as->as_npages1 * PAGE_SIZE;
	vbase2 = as->as_vbase2;
	vtop2 = vbase2 + as->as_npages2 * PAGE_SIZE;
	stackbase = USERSTACK - DUMBVM_STACKPAGES * PAGE_SIZE;
	stacktop = USERSTACK;

	if (faultaddress >= vbase1 && faultaddress < vtop1) {
		// Memory is in text section, so readonly
		dirtiable = false;
		// Calculate physical address from coremap
		paddr = vaddr_to_paddr(faultaddress,  vbase1, as->as_pbase1);
	}
	else if (faultaddress >= vbase2 && faultaddress < vtop2) {
		// Memory isn't in text section, so dirtiable
		dirtiable = true;
		// Calculate physical address from coremap
		paddr = vaddr_to_paddr(faultaddress,  vbase2, as->as_pbase2);
	}
	else if (faultaddress >= stackbase && faultaddress < stacktop) {
		// Memory isn't in text section, so dirtiable
		dirtiable = true;
		// Calculate physical address from coremap
		paddr = vaddr_to_paddr(faultaddress, stackbase, as->as_stackpbase);
	}
	else {
		return EFAULT;
	}

	// Check if addressSpace is done loading
	if(!as->loaded) dirtiable = true;

	/* make sure it's page-aligned */
	KASSERT((paddr & PAGE_FRAME) == paddr);

	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_read(&ehi, &elo, i);
		if (elo & TLBLO_VALID) {
			continue;
		}
		ehi = faultaddress;
		// If the current page is dirtiable, set TLBLO_DIRTY, otherwise 0
		elo = paddr | (dirtiable ? TLBLO_DIRTY : 0) | TLBLO_VALID;
		DEBUG(DB_VM, "dumbvm: 0x%x -> 0x%x\n", faultaddress, paddr);
		tlb_write(ehi, elo, i);
		splx(spl);
		return 0;
	}

#if OPT_A3
	// TLB is full so write to random
	ehi = faultaddress;
	elo = paddr | (dirtiable ? TLBLO_DIRTY : 0) | TLBLO_VALID;
	tlb_random(ehi,elo);
	splx(spl);
	return 0;
#else
	kprintf("dumbvm: Ran out of TLB entries - cannot handle page fault\n");
	return EFAULT;
#endif

}

struct addrspace *
as_create(void)
{
	struct addrspace *as = kmalloc(sizeof(struct addrspace));
	if (as==NULL) {
		return NULL;
	}
	#if OPT_A3

	as->as_vbase1 = 0;
	as->as_pbase1 = 0;
	as->as_npages1 = 0;
	as->as_vbase2 = 0;
	as->as_pbase2 = 0;
	as->as_npages2 = 0;
	as->as_stackpbase = 0;
	#endif
	return as;
}

// For converting virtual addresses to physical addresses in the coremap
paddr_t vaddr_to_paddr(vaddr_t vaddr, vaddr_t vbase, paddr_t pbase) {
	//Calculate page index in coremap
	int pageIdx = (pbase - firstPage) / PAGE_SIZE;
	spinlock_acquire(&coremap_lock);
	// Parse through all physical pages until vaddr is reached
	for (size_t i = 0; i < (vaddr - vbase) / PAGE_SIZE; i++) {
		pageIdx = coremap[pageIdx].nextPage;
	}
	spinlock_release(&coremap_lock);
	return (paddr_t)(firstPage + (pageIdx * PAGE_SIZE) + (vaddr % PAGE_SIZE));
}

void
as_destroy(struct addrspace *as)
{
	#if OPT_A3

	free_kpages(PADDR_TO_KVADDR(as->as_pbase1));
	free_kpages(PADDR_TO_KVADDR(as->as_pbase2));
	free_kpages(PADDR_TO_KVADDR(as->as_stackpbase));

	#endif
	kfree(as);
}

void
as_activate(void)
{
	int i, spl;
	struct addrspace *as;

	as = curproc_getas();
#ifdef UW
        /* Kernel threads don't have an address spaces to activate */
#endif
	if (as == NULL) {
		return;
	}


	/* Disable interrupts on this CPU while frobbing the TLB. */
	spl = splhigh();

	for (i=0; i<NUM_TLB; i++) {
		tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
	}

	splx(spl);
}

void
as_deactivate(void)
{
	/* nothing */
}

int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t sz,
		 int readable, int writeable, int executable)
{
	size_t npages; 

	/* Align the region. First, the base... */
	sz += vaddr & ~(vaddr_t)PAGE_FRAME;
	vaddr &= PAGE_FRAME;

	/* ...and now the length. */
	sz = (sz + PAGE_SIZE - 1) & PAGE_FRAME;

	npages = sz / PAGE_SIZE;

	/* We don't use these - all pages are read-write */
	(void)readable;
	(void)writeable;
	(void)executable;
	#if OPT_A3
		if(as->)
	#else

	if (as->as_vbase1 == 0) {

		as->as_vbase1 = vaddr;
		as->as_npages1 = npages;
		return 0;
	}

	if (as->as_vbase2 == 0) {
		as->as_vbase2 = vaddr;
		as->as_npages2 = npages;
		return 0;
	}
	#endif
	/*
	 * Support for more than two regions is not available.
	 */
	kprintf("dumbvm: Warning: too many regions\n");
	return EUNIMP;
}

static
void
as_zero_region(paddr_t paddr, unsigned npages)
{
	bzero((void *)PADDR_TO_KVADDR(paddr), npages * PAGE_SIZE);
}

int
as_prepare_load(struct addrspace *as)
{
	KASSERT(as->as_pbase1 == 0);
	KASSERT(as->as_pbase2 == 0);
	KASSERT(as->as_stackpbase == 0);

	as->as_pbase1 = getppages(as->as_npages1);
	if (as->as_pbase1 == 0) {
		return ENOMEM;
	}

	as->as_pbase2 = getppages(as->as_npages2);
	if (as->as_pbase2 == 0) {
		return ENOMEM;
	}

	as->as_stackpbase = getppages(DUMBVM_STACKPAGES);
	if (as->as_stackpbase == 0) {
		return ENOMEM;
	}
	
	as_zero_region(as->as_pbase1, as->as_npages1);
	as_zero_region(as->as_pbase2, as->as_npages2);
	as_zero_region(as->as_stackpbase, DUMBVM_STACKPAGES);

	return 0;
}

int
as_complete_load(struct addrspace *as)
{
	//(void)as;
	as->loaded = true;
	return 0;
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{
	KASSERT(as->as_stackpbase != 0);

	*stackptr = USERSTACK;
	return 0;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *new;

	new = as_create();
	if (new==NULL) {
		return ENOMEM;
	}

	new->as_vbase1 = old->as_vbase1;
	new->as_npages1 = old->as_npages1;
	new->as_vbase2 = old->as_vbase2;
	new->as_npages2 = old->as_npages2;

	/* (Mis)use as_prepare_load to allocate some physical memory. */
	if (as_prepare_load(new)) {
		as_destroy(new);
		return ENOMEM;
	}

	KASSERT(new->as_pbase1 != 0);
	KASSERT(new->as_pbase2 != 0);
	KASSERT(new->as_stackpbase != 0);


	memmove((void *)PADDR_TO_KVADDR(new->as_pbase1),
		(const void *)PADDR_TO_KVADDR(old->as_pbase1),
		old->as_npages1*PAGE_SIZE);

	memmove((void *)PADDR_TO_KVADDR(new->as_pbase2),
		(const void *)PADDR_TO_KVADDR(old->as_pbase2),
		old->as_npages2*PAGE_SIZE);

	memmove((void *)PADDR_TO_KVADDR(new->as_stackpbase),
		(const void *)PADDR_TO_KVADDR(old->as_stackpbase),
		DUMBVM_STACKPAGES*PAGE_SIZE);
	
	*ret = new;
	return 0;
}
