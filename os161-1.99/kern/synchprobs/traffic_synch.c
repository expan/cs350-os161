#include <types.h>
#include <lib.h>
#include <synchprobs.h>
#include <synch.h>
#include <opt-A1.h>
#include <array.h>

/* 
 * This simple default synchronization mechanism allows only vehicle at a time
 * into the intersection.   The intersectionSem is used as a a lock.
 * We use a semaphore rather than a lock so that this code will work even
 * before locks are implemented.
 */

/* 
 * Replace this default synchronization mechanism with your own (better) mechanism
 * needed for your solution.   Your mechanism may use any of the available synchronzation
 * primitives, e.g., semaphores, locks, condition variables.   You are also free to 
 * declare other global variables if your solution requires them.
 */

/*
 * replace this with declarations of any synchronization and other variables you need here
 */
static struct cv *intersectionCv;
static struct lock *intersectionLk;
struct array *vehiclesAtIntersection;
volatile int numVehiclesInIntersection = 0;

// Define vehicle struct
typedef struct Vehicle
{
  Direction origin;
  Direction destination;
} Vehicle;

// Forward declaration
bool right_turn(Vehicle *v);
bool check_constraints(Vehicle *vNew, Vehicle *vOld);

/*
 * bool right_turn()
 * 
 * Purpose:
 *   predicate that checks whether a vehicle is making a right turn
 *
 * Arguments:
 *   a pointer to a Vehicle
 *
 * Returns:
 *   true if the vehicle is making a right turn, else false
 *
 * Note: written this way to avoid a dependency on the specific
 *  assignment of numeric values to Directions
 */
bool right_turn(Vehicle *v) {
  KASSERT(v != NULL);
  if (((v->origin == west) && (v->destination == south)) ||
      ((v->origin == south) && (v->destination == east)) ||
      ((v->origin == east) && (v->destination == north)) ||
      ((v->origin == north) && (v->destination == west))) {
    return true;
  } else {
    return false;
  }
}

/*
 * bool check_constraints()
 * 
 * Purpose:
 *   checks whether the entry of a vehicle into the intersection violates
 *   any synchronization constraints.   Returns true if the two vehicles are not conflicting each other and 
    false otherwise
 *
 * Arguments:
 *   Two vehicles
 *
 * Returns:
 *   true if the two vehicles are not conflicting each other and false otherwise
 */
bool check_constraints(Vehicle *vNew, Vehicle *vOld){
  if(vNew->origin == vOld->origin)
    return true;
  else if((vNew->origin == vOld->destination) && (vNew->destination == vOld->origin))
    return true;
  else if((vNew->destination != vOld->destination) && (right_turn(vNew) || right_turn(vOld)))
    return true;
  else 
    return false;
}

/* 
 * The simulation driver will call this function once before starting
 * the simulation
 *
 * You can use it to initialize synchronization and other variables.
 * 
 */
void
intersection_sync_init(void)
{
  /* replace this default implementation with your own implementation */
  intersectionCv = cv_create("intersectionCv");
  intersectionLk = lock_create("intersectionLk");
  vehiclesAtIntersection = array_create();
  array_init(vehiclesAtIntersection);
  if (intersectionCv == NULL) {
    panic("could not create intersection cv");
  }

  if (intersectionLk == NULL) {
    panic("could not create intersection lock");
  }

  if (vehiclesAtIntersection == NULL) {
    panic("could not create array");
  }
  return;
}

/* 
 * The simulation driver will call this function once after
 * the simulation has finished
 *
 * You can use it to clean up any synchronization and other variables.
 *
 */
void
intersection_sync_cleanup(void)
{
  /* replace this default implementation with your own implementation */
  KASSERT(intersectionCv != NULL);
  cv_destroy(intersectionCv);

  KASSERT(intersectionLk != NULL);
  lock_destroy(intersectionLk);

  KASSERT(vehiclesAtIntersection != NULL);
  array_destroy(vehiclesAtIntersection);
}


/*
 * The simulation driver will call this function each time a vehicle
 * tries to enter the intersection, before it enters.
 * This function should cause the calling simulation thread 
 * to block until it is OK for the vehicle to enter the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle is arriving
 *    * destination: the Direction in which the vehicle is trying to go
 *
 * return value: none
 */

void
intersection_before_entry(Direction origin, Direction destination) 
{
  /* replace this default implementation with your own implementation */
  KASSERT(intersectionCv != NULL);
  KASSERT(intersectionLk != NULL);
  KASSERT(vehiclesAtIntersection != NULL);

  // Create new vehicle
  Vehicle *v = kmalloc(sizeof(struct Vehicle));
  v->origin = origin;
  v->destination = destination;

  lock_acquire(intersectionLk);
  
  for(unsigned int i=0; i < array_num(vehiclesAtIntersection); i++){
      // If the new vehicle conflicts with something already in the vehicle stack
      //  can't add so block until you can add it
      if(!check_constraints(v,array_get(vehiclesAtIntersection,i))){
        while(numVehiclesInIntersection > 0){
          cv_wait(intersectionCv, intersectionLk);
        }
        break;
      }
  }

  // Add vehicle to intersection
  numVehiclesInIntersection++;
  array_add(vehiclesAtIntersection,v, NULL);

  lock_release(intersectionLk);
  //P(intersectionSem);
}


/*
 * The simulation driver will call this function each time a vehicle
 * leaves the intersection.
 *
 * parameters:
 *    * origin: the Direction from which the vehicle arrived
 *    * destination: the Direction in which the vehicle is going
 *
 * return value: none
 */

void
intersection_after_exit(Direction origin, Direction destination) 
{
  /* replace this default implementation with your own implementation */
  KASSERT(intersectionCv != NULL);
  KASSERT(intersectionLk != NULL);
  KASSERT(vehiclesAtIntersection != NULL);

  lock_acquire(intersectionLk);

  // Look for the exiting vehicle
  for(unsigned int i=0; i < array_num(vehiclesAtIntersection); i++){
    Vehicle *cur = array_get(vehiclesAtIntersection, i);

    if((cur->origin == origin) && (cur->destination == destination)){
      array_remove(vehiclesAtIntersection,i);
      numVehiclesInIntersection--;
      // Broadcast to all vehicles that something exited
      cv_broadcast(intersectionCv, intersectionLk);
      break;
    }
  }

  lock_release(intersectionLk);

}
